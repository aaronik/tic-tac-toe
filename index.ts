import shardusDB from 'shardus-db'

type TileState = 0 | 1 | 2 // no mark, p1 mark, p2 mark
type Turn = 1 | 2

type GameState = {
  board: {
    tl: TileState, t: TileState, tr: TileState,
    l:  TileState, m: TileState, r:  TileState,
    bl: TileState, b: TileState, br: TileState
  }
  turn: Turn
}

const db = shardusDB<GameState>()

const generateCleanGameBoard = (): GameState['board'] => {
  return {
    tl: 0 as TileState, t: 0 as TileState, tr: 0 as TileState,
    l:  0 as TileState, m: 0 as TileState, r:  0 as TileState,
    bl: 0 as TileState, b: 0 as TileState, br: 0 as TileState
  }
}

const fail = (res, reason: string) => res.json({ success: false, reason })

// For later tx safety
const tileStrings = ['tl', 't', 'tr', 'l', 'm', 'r', 'bl', 'b', 'br']

db.shardus.registerExternalPost('initialize', (req, res) => {
  // Initialize game state
  const dbRes = db.set({
    board: generateCleanGameBoard(),
    turn: 1
  })

  res.json(dbRes)
})

db.shardus.registerExternalPost('move', (req, res) => {
  const { player, tile } = req.body

  // Do a little validation.. Emphasis on little
  if (!player)                     return fail(res, 'You must supply a player')
  if (![0, 1, 2].includes(player)) return fail(res, `Player must be 0, 1 or 2, you gave: ${player} [${typeof player}`)
  if (!tile)                       return fail(res, 'You must supply a title')
  if (!tileStrings.includes(tile)) return fail(res, 'Title must be valid, you supplied: ' + tile)

  // Getting our whole state is as easy as this.
  const gameState = db.get()

  // One more validation for safety's sake
  if (gameState.turn != player) return fail(res, 'It\'s not your turn, guy!')

  // Make that move
  gameState.board[tile] = player

  // toggle that turn
  gameState.turn = gameState.turn == 1 ? 2 : 1

  // Make the write! 1-2-3, easy peasy.
  const dbRes = db.set(gameState)

  res.json(dbRes)
})

db.shardus.registerExternalGet('game', (req, res) => {
  // Get our whole state.
  const gameState = db.get()
  res.json(gameState)
})

// Snuck this in here so we can see shardusDB's internal representation of our
// state in `accounts` form
db.shardus.registerExternalGet('accounts', (req, res) => {
  res.json(db._accounts)
})
