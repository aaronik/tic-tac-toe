import * as Vorpal from 'vorpal'
const vorpal = Vorpal()
import * as got from 'got'

// Talking with the server consists of sending HTTP GET requests and HTTP POST
// requests. The latter sends data in the shape of a Transaction. You'll see
// the abbreviation 'tx'. We call this "injecting a transaction".
const URL = 'http://localhost:9001'

async function postJSON(url, obj): Promise<string> {
  const response = await got.post(url, {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify(obj)
  })
  return response.body
}

vorpal
  .command('game', 'Get the state of the game.')
  .action(async (args, cb) => {
    const res = await got(URL + '/game')
    vorpal.log(JSON.parse(res.body))
    cb()
  })

vorpal
  .command('accounts', 'Get the low level accounts object from shardusDB')
  .action(async (args, cb) => {
    const res = await got(URL + '/accounts')
    vorpal.log(JSON.parse(res.body))
    cb()
  })

vorpal
  .command('initialize', 'Initializes the game board. Do this before you play a round.')
  .action(async (args, callback) => {
    const json = {}

    const res = await postJSON(URL + '/initialize', json)
    vorpal.log(JSON.parse(res))
    callback()
  })

  vorpal
  .command('move <player> <tile>', 'Makes a move for player <player: 1 | 2> for <tile tl | t | tr | l | m | r | bl | b | br>')
  .action(async (args, callback) => {
    const json = {
      player: args.player,
      tile: args.tile
    }

    const res = await postJSON(URL + '/move', json)
    vorpal.log(JSON.parse(res))
    callback()
  })

vorpal
  .delimiter('client$')
  .show()
