# Tic Tac Toe

A simple app to help build / demonstrate
[shardusDB](https://gitlab.com/shardus/applications/shardus-db)

## Installation

```sh
git clone git@gitlab.com:shardus/applications/tic-tac-toe.git
cd tic-tac-toe
npm install
```

## Usage

```sh
npm start
```

And in another terminal

```sh
npm run client
```

Now you can interact with the client

```sh
client$ initialize # Set up the game board
client$ game # See the board and player turn (The whole game state)
client$ move 1 bl # Have player 1 put their mark on the Bottom Left tile.
```

## TODO

This is quite a thin implementation! There are lots of things still to do.
Check out [the official
docs](https://shardus.gitlab.io/docs/developer/examples/tic-tac-toe-template.html)
for ideas.
