// TODO use native library if possible
const { rm } = require('shelljs')

async function main () {
  try {
    rm('-rf','./.pm2 ./db ./logs ./statistics.tsv ./archiver-db* ./archiver-logs ./monitor-logs ./db-old-*'.split(' '))
    rm('-rf', './archiver-db.sqlite'.split(' '))
  } catch (e) {
    console.log(e)
  }
}
main()
